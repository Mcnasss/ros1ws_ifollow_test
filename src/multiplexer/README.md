## Multiplexer (time : 240 min)

This repository is a ROS package helping to multiplexe a two sources velocity command. Let's remember that ROS1 already provides a node `mux` that suscribe to a set of incoming data from one of them to another topic. Here, just basic re-implementation.

# Installation
1. Clone the repository into `src/` directory of ROS workspace
2. Install all dépendancies with rosdep
3. Compile the workspace

# Execution
```
roslaunch mux_pkg package.launch
```

# Use
Before to use this package, we need to set in configuration file located in `config/package.json` all `n input topics` and `output topic` of velocity. Please refers to that to specify the topics to use (here, /cmd_local , /cmd_web).

**Note :** Here is no specific logic implementation to set a high order velocity source for switch control. So, to switch between two control source, simply publish one topic or another in two different terminal to make the switch. For exemple :
```
(1)
 rostopic pub -r 1 cmd_local geometry_msgs/Twist "linear:
  x: 1.0
  y: 2.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 4.0"
```

```
(2)
rostopic pub -r 1 /cmd_web geometry_msgs/Twist "linear:
  x: 7.0
  y: 3.5
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 2.0"
```
```
(3) Visualise with
rostopic echo /cmd_vel
```

**Remark :** The switch is based on the difference of velocity received through topics. We can also create a `custom ros msg` that will integrate :
```
1- the name/id of the source cmd
2- the velocity command data
```
and apply the switch based on these name/id

The following picture show in different terminal the velocity command ant the result as output velocity

![alt text](../../pictures/multiplexer.png "Velocity cmd from two control source and output")

## Support
Please contact Nassim Sadikou for more details : mcnass2@gmail.com

