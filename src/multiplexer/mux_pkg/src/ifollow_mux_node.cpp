#include <iostream>
#include <string.h>
#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
using namespace std;

class ParametersROS 
{
    private:
    string prefix;

    public:
    string outopic, topic1, topic2;
    int frequency;

    ParametersROS(ros::NodeHandle &_nh) 
    {
      if(_nh.hasParam("/ifollow")) { 
        this->prefix = "/ifollow/"; 
      }
      else if(_nh.hasParam("mux")) { 
        this->prefix = "/"; 
      }
      else { 
         ROS_INFO("Error, no parameters to load"); return; 
      }

      _nh.getParam(this->prefix+"mux/outopic", this->outopic);
      _nh.getParam(this->prefix+"mux/topic1", this->topic1);
	_nh.getParam(this->prefix+"mux/topic2", this->topic2);
      _nh.getParam(this->prefix+"mux/frequency", this->frequency);
    }
};

class Mux
{
	private:
	ros::NodeHandle nh;
	geometry_msgs::Twist cmd;
	ros::Publisher mux_pub;
    	ros::Subscriber mux_sus1, mux_sus2;

	void multiplex(const geometry_msgs::Twist & cmd_vel)
	{
		if(!isSame(cmd_vel, this->cmd)){
		this->cmd = cmd_vel;}
		this->mux_pub.publish(this->cmd);
    	}

	bool isSame(const geometry_msgs::Twist & cmd1, 
			const geometry_msgs::Twist & cmd2)
	{
		return (cmd1.linear.x != cmd2.linear.x) ? 0 :
		 	 (cmd1.linear.y != cmd2.linear.y) ? 0 :
		 	 (cmd1.angular.z != cmd2.angular.z) ? 0 :
									1 ;
	}

  	public:
    	ros::Rate loop_rate = 60;
   	Mux(ros::NodeHandle &_nh, ParametersROS &_param) : nh(_nh){
      this->loop_rate = _param.frequency;
      this->mux_sus1 = nh.subscribe(_param.topic1, 1, &Mux::onLocalCmd, this);
	this->mux_sus2 = nh.subscribe(_param.topic2, 1, &Mux::onWebCmd, this);
	this->mux_pub = nh.advertise<geometry_msgs::Twist>(_param.outopic, 100);
    	}

    	~Mux(void) {}

	void onLocalCmd(const geometry_msgs::Twist& cmd){
		multiplex(cmd);
    	}

	void onWebCmd(const geometry_msgs::Twist& cmd){
		multiplex(cmd);
    	}
};


int main(int argc, char **argv) 
{
  ros::init(argc, argv, "random");
  ros::NodeHandle nh;
  ParametersROS param(nh);
  Mux mux(nh, param);
  ros::spin();
  mux.~Mux();
  return 0;
}



