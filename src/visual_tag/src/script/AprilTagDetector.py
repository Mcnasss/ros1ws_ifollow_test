import cv2
import apriltag

class Detector:
    def __init__(self):
        self.image = None
        self.results = None
        self.detector = None
        self.successProcessing = False

    def perfomDetection(self, filepath:str, dim_thresh:int) -> None:
        img = cv2.imread(filepath)
        self.image = img.copy()
	  
	  # detection can fail on too large image
        if self.image.shape[0] > dim_thresh or self.image.shape[1] > dim_thresh:
            self.image = cv2.resize(self.image, (dim_thresh, dim_thresh))

        print("[INFO] Performing AprilTags detection...")
        gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        options = apriltag.DetectorOptions(families="tag36h11")
        self.detector = apriltag.Detector(options)
        self.results = self.detector.detect(gray)
        print("[INFO] {} AprilTags detected".format(len(self.results)))
        
    def process(self, path:str, dim_thresh:int) -> bool:
        try:
            self.perfomDetection(path, dim_thresh)
            if len(self.results) !=0 :
                for r in self.results:
                    # bounding box (x, y) coordinate
                    (ptA, ptB, ptC, ptD) = r.corners
                    ptB = (int(ptB[0]), int(ptB[1]))
                    ptC = (int(ptC[0]), int(ptC[1]))
                    ptD = (int(ptD[0]), int(ptD[1]))
                    ptA = (int(ptA[0]), int(ptA[1]))

                    # Boundaries box (BGR | (o) top left, (y) to down, (x) to right
                    cv2.line(self.image, ptA, ptB, (255, 0, 0), 2)
                    cv2.line(self.image, ptB, ptC, (0, 0, 255), 2)
                    cv2.line(self.image, ptC, ptD, (0, 255, 0), 2)
                    cv2.line(self.image, ptD, ptA, (0, 255, 0), 2)

                    # Ccenter (x, y) coordinaite of the AprilTag
                    (cX, cY) = (int(r.center[0]), int(r.center[1]))
                    cv2.circle(self.image, (cX, cY), 5, (0, 0, 255), -1)

                    tagFamily = r.tag_family.decode("utf-8")
                    cv2.putText(self.image, tagFamily, (ptA[0], ptA[1] -15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    print("[INFO] ARTag family {} \n".format(tagFamily))
                    self.successProcessing = True
            else:
                print("[INFO] No feature detected")
                self.successProcessing = False

        except Exception as e:
            print(e)
            self.successProcessing = False

        finally:
            if self.successProcessing:
                cv2.imshow("AprilTag", self.image)
                cv2.waitKey(dim_thresh*5)
            cv2.destroyAllWindows()
            return True
        
