#!/usr/bin/env python3
import glob
import rospy
from pathlib import Path
from std_msgs.msg import Bool
from AprilTagDetector import Detector
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import PoseWithCovarianceStamped

class Parameters:
    def __init__(self):
        self.tags = None
        self.directory = None
        
class ParametersROS(Parameters):
    def __init__(self):
        super().__init__()
        self.frequency = None
        self.tag_goal_topic = None
        self.start_tag_topic = None

    def fromROS(self, prefix="~") -> None:
        self.tags = rospy.get_param(prefix+"tags")
        self.directory = rospy.get_param(prefix+"directory")
        self.frequency = rospy.get_param(prefix+"frequency")
        self.tag_goal_topic = rospy.get_param(prefix+"tag_goal_topic")
        self.start_tag_topic = rospy.get_param(prefix+"start_tag_topic")
        
    def fromDebug(self) -> None: #TODO: Remove debug and use ROS parameter
        self.start_tag_topic = "artag_goal"
        self.tag_goal_topic = "move_base_simple/goal"
        self.frequency = 60
        self.directory = "tags"
        self.tags = {"1" : {"file" : "ar_tag_1.JPG", "pos_x" : 1.0, "pos_y" : 2.0, "pos_theta" : 1.57}, "2" : {"file" : "ar_tag_2.JPG", "pos_x" : 2.0, "pos_y" : 1.0, "pos_theta" : 3.14}, "3" : {"file" : "ar_tag_3.JPG", "pos_x" : 3.0, "pos_y" : 3.0,"pos_theta" : 1.57}}


class Artag:
    def __init__(self, param:ParametersROS):
        self.detector = Detector()
        self.tags_features = []
        [self.tags_features.append(i) for i in param.tags.values()]
        self.dir = "/"+param.directory+"/"

    def exec(self, path:str, thresh:int) -> bool:
        return self.detector.process(path, thresh)


def onTagGoal_clbk(msg) -> None:
    rpyGoal = PoseStamped()
    for tag in artag.tags_features:
        path = "".join(glob.glob(str(Path().absolute())))[:-5]+"/ros1ws_ifollow_test" #TODO: HardCoded, use rospack()
        if artag.exec(path+artag.dir+tag['file'], 800) : 
            rpyGoal.header.frame_id = "map"
            rpyGoal.header.stamp = rospy.get_rostime()
            rpyGoal.pose.position.x = tag['pos_x']
            rpyGoal.pose.position.y = tag['pos_y']
            rpyGoal.pose.orientation.w = 0 #TODO: required convert yaw -> quaternion 
            rpyGoal.pose.orientation.x = 0
            rpyGoal.pose.orientation.y = 0
            rpyGoal.pose.orientation.z = tag['pos_theta']
        goal2D_publisher.publish(rpyGoal)

if __name__ == "__main__":
    rospy.init_node('random')
    param = ParametersROS()
    param.fromDebug()
    rate = rospy.Rate(param.frequency)
    artag = Artag(param)
    start_subscriber = rospy.Subscriber(param.start_tag_topic, Bool, onTagGoal_clbk)
    goal2D_publisher = rospy.Publisher(param.tag_goal_topic, PoseStamped, queue_size=1)
    rate.sleep()
    rospy.spin()
