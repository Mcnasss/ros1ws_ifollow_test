## Visual tag (time : 600 min)

This repository is a ROS package which send a nav goal through an AprilTag detection.

![alt text](../../pictures/tag.png "AprilTag")

# Installation
1. Clone the repository into `src/` directory of ROS workspace
2. Install all dépendancies with rosdep
3. Compile the workspace

# Prerequisites
```
pip3 install apriltag (if not installed)
```

# Execution:
Based on the configuration file or your package at `config/package.json`, set the `directory`, `number of AprilTag files`, `filename`, `pos_x`, `pos_y`, `pos_theta` corresponding to the tags :
```
    "directory" : "tags",
    "tags" : 
    {
      "1" :
      {
        "file": "ar_tag_1.JPG",
        "pos_x": 3.0,
        "pos_y": 4.0,
        "pos_theta": 1.57
      },
      "2" : 
      {
        "file": "ar_tag_2.JPG",
        "pos_x": 6.0,
        "pos_y": 8.0,
        "pos_theta": 3.14
      },
      "3" : 
      {
        "file" : "ar_tag_3.JPG",
        "pos_x" : 9.0,
        "pos_y" : 12.0,
        "pos_theta" : 4.73
      }
    }
```
Then,
1) Launch the package
```
roslaunch visual_tag package.launch
```
2) Trigger the detection process
```
rostopic pub /artag_goal std_msgs/Bool "data: true"
```
3) Listen the nav goal published
```
rostopic echo /move_base_sple/goal
```

# Explanation
Any robot on ROS whis is suscribing to nav goal topic through RVIZ will go to the goal. 
In case of multiple AprilTags, in this software, we get the Pose callback of the robot when the goal is reached before to send the next goal corresponding to the AprilTag.

## Support
Please contact Nassim Sadikou for more details : mcnass2@gmail.com

