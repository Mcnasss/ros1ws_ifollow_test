# (copyright : Nassim Sadikou) #
import random
import argparse
import sys,tty,termios
from paho.mqtt import client as mqtt_client

# construct the argument parser and parse it
ap = argparse.ArgumentParser()
ap.add_argument("-host", "--host", required=True, help="host")
ap.add_argument("-port", "--port", required=True, help="port")
ap.add_argument("-out", "--pub", required=True, help="topic published in")
ap.add_argument("-in", "--sus", required=True, help="topic subscribed in")
args = vars(ap.parse_args())

# Print for raw terminal mode
def println(*args):
    print(*args, end='\r\n', flush=True)


               ########### MQTT MANAGE ##########
broker = args["host"]
port = int(args["port"])
broker_in = args["pub"]
broker_out = args["sus"]
client_id = f'python-mqtt-{random.randint(0, 1000)}'

### connect ###
def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("************* Loading.... ************")
            #print("* Remote robot teleoperation ready **")
            print("**********         ^         *********")
            print("********  <    [Ctrl+X]    >  ********")
            print("**********         v         *********")
        else:
            print("Failed to connect, return code %d\n", rc)
    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

### suscriber ###
def subscribe_mqtt(client: mqtt_client):
    def on_message(client, userdata, msg):
        println(f"Command success `{msg.payload.decode()}`")
        ## BONUS : make a sound ##
    client.subscribe(broker_out)
    client.on_message = on_message

### publisher ###
def publish_mqtt(client:mqtt_client, msg:str) :
    result = client.publish(broker_in, '{"data": "%s"}'%msg)
    status = result[0]

                   ####### KEYBOARD MANAGE ########
# Commands codes
END_OF_TEXT = chr(3)  # CTRL+C (print nothing - Exit)
END_OF_FILE = chr(4)  # CTRL+D (print nothing - Exit)
CANCEL      = chr(24) # CTRL+X (print nothing - Exit)

# Escape code and sequences for terminal keyboard navigation
ESCAPE      = chr(27)
CONTROL     = ESCAPE +'['
ARROW_UP    = CONTROL+'A'
ARROW_DOWN  = CONTROL+'B'
ARROW_RIGHT = CONTROL+'C'
ARROW_LEFT  = CONTROL+'D'
KEY_END     = CONTROL+'F'
KEY_HOME    = CONTROL+'H'
PAGE_UP     = CONTROL+'5~'
PAGE_DOWN   = CONTROL+'6~'

# Escape sequences to match
commands = {
    ARROW_UP   :'Up',
    ARROW_DOWN :'Down',
    ARROW_RIGHT:'Right',
    ARROW_LEFT :'Left'
}

# 1- Blocking read of one input character
# 2- Detecting appropriate interrupts
def getch():
    k = sys.stdin.read(1)[0]
    if k in {END_OF_TEXT, END_OF_FILE} :
        raise KeyboardInterrupt
    elif k is CANCEL:
        return "S"
    return k

# Preserve current terminal settings 
# (we will restore these before exiting)
fd = sys.stdin.fileno()
old_settings = termios.tcgetattr(fd)


                       ####### MAIN #######
if __name__ == "__main__":
    try:
        client = connect_mqtt()
        client.loop_start()
        
        # Enter raw mode (key events sent directly as characters)
        tty.setraw(sys.stdin.fileno())
        
        # Loop, waiting for keyboard input
        while 1:
            subscribe_mqtt(client)
            read = getch() # Parse known command escape sequences
            while any(k.startswith(read) for k in commands.keys()):
                print(read)
                if read in commands:
                    publish_mqtt(client, commands[read])
                    println('command (%s)' %commands[read])
                    read = ''
                    break
                read += getch()
            # Interpret all other inputs as text input
            for c in read:
                if c is "S":
                    publish_mqtt(client, "Stop")
                    println('command (Stop)')
                    read = ''
                    break
                #println('ignored command 0x%X %c'%(ord(c),c))

    #  Clean up
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings) 
        println('')
        sys.exit(0)
    
