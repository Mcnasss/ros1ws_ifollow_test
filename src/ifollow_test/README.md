## Setting test environment (time : 420 min)

This repository is a package which contains all the configuration files required for the workspace to be ran successfully. It also contains the `remote teleoperation application` to operate through web with the robot.

# Installation
1. Clone the repository into `src/` directory of ROS workspace
2. Install all dépendancies with rosdep
3. Compile the workspace

# Execution
```
roslaunch ifollow_test ifollow_view.launch map_file:=$HOME/ros1ws_ifollow_test/maps/map.yaml
```

## Support
Please contact Nassim Sadikou for more details : mcnass2@gmail.com
