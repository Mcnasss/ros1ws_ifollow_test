## Remote teleoperation (time : 240 min)

This repository provides a package to perform a remote teleoperation with TB3 in simulation.

## Principe
It allow communication back and forth between ROS and any MQTT client through a MQTT Broker, here Mosquitto.

It include a ROS MQTT_bridge package which help to instantiate the MQTT broker which set the dialog channel.

# Installation
1. Clone the repository into `src/` directory of ROS workspace
2. Install all dépendancies with rosdep
3. Compile the workspace


## Prerequisites
**1-** We assume to have a Mosquitto broker running with the default configuration and listening to port 1883. It that not the case, install it. `In a new terminal :`

```
$ lsb_release -cd
$ getconf LONG_BIT
$ sudo apt udpate
$ sudo apt install snapd -y
$ snap install mosquitto
```

**Remark :**  To check if everything is ok, `in two different terminal`, try the following message exchange on your local host through the mqtt topic `test`.
```
(T1) $ mosquitto_sub -h localhost -t test

(T2) $ mosquitto_pub -h localhost -t test -m "Hello world"
```

**2-** Now, Install ROS MQTT_bridge inside you workspace. To do this, `in a new terminal` :
```
$ cd ~/ros1ws_ifollow_test/src

$ git clone https://github.com/groove-x/mqtt_bridge.git
```

**Remark :** Some steps are required to make the package work properly. If you follow the github README, you need to install the following dependancies. `In new terminal :`

```
$ sudo apt install mosquitto mosquitto-clients

$ sudo apt install ros-melodic-rosbridge-library

$ sudo apt install python3-pip

$ cd ~/ros1ws_ifollow_test/src/mqtt_bridge/

$ pip3 install -r dev-requirements.txt
```

**Remark :** If an error occurs, remove from `dev-requirements.txt` the uncessary package line by adding comment such `#git+https://github.com/RobotWebTools`


# Execution
To test the package, `in three different terminal`, try :
```
(T1) $ roslaunch mqtt_bridge package.launch
```

**ROS publisher (each 1 sec) -> MQTT subscriber**
```
(T2) $ rostopic pub -r 1 /cmd_broker_out std_msgs/Bool "data: true"
(T3) $ mosquitto_sub -h localhost -t cmd_mqtt_in
```
**MQTT publisher -> ROS subscriber**
```
(T2) $ mosquitto_pub -h localhost -t cmd_mqtt_out -m '{"data": "Up"}'
(T3) $ rostopic echo /cmd_broker_in
```
`The published messages should respectively appears in (T3)`

**Remark :** If an error `unpack(b) received extra data` occurs when deserealize msg from MQTT to ROS, you need to edit your `demo_params.yaml` file by editing 
```
this :
serializer: msgpack: dumps
deserializer: msgpack: loads

to this:
serializer: json: dumps
deserializer: json: loads
```

That file also include include the configuration for all channel communication you would like to set. You can custom it. You can also create a custom broker class by inheriting `mqtt_brige.bridge.Bridge`

**NB :** If an other error occur, rebuild the whole workspace to fix the package

![alt text](../../pictures/broker.png "Broker package test")


## FURTHER


## Support
Please contact Nassim Sadikou for more details : mcnass2@gmail.com

