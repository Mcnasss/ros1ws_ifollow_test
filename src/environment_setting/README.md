## Setting test environment (time : 420 min)

This repository show the installation process of the test environnement for `ifollow test`. It also includes few packages useful to control the TB3 burger in simulation.


## Compilation
1. Clone the repository into `src/` directory of ROS workspace
2. Install all dépendancies with rosdep

To run turlebot in simulation require to download this package and build the workspace. `In new terminal` :
```
$ cd ~/ros1ws_ifollow_test/src/environment_setting/

$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git

$ git clone https://github.com/ROBOTIS-GIT/turtlebot3.git

$ git clone https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git

$ cd ~/ros1ws_ifollow_test && catkin_make
```

**Remark :** If an error occur, `call rosdep install for specifc dependencies command` as shown at the step ROS Dependance above to fix the error.

## Execution
To visualize the robot inside a map in RVIZ :

(1)- at first, build a map using a gazebo model in rviz through gmapping. Then, save the map and display it with robot. `In new terminal` :
**a- Start Gazebo node with the model you prefer (here simply world model)**
```
> roslaunch turtlebot3_gazebo turtlebot3_world.launch
```

**b- Start simultaneously gmapping to draw the map using RVIZ.**
```
> roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping
```

**c- Start autonomous naviguation to build the map drawn.**
```
> roslaunch turtlebot3_gazebo turtlebot3_simulation.launch
```

**e- Save the map into a directory `maps`. First ensure to have map server package installed**
```
> sudo apt install ros-melodic-map-server (if not installed)

> rosrun map_server map_saver -f ~/ros1ws_ifollow_test/maps/map assuming the directory `maps` exist in your workspace root
```

## Now, visualize the robot in RVIZ with map.

`For that, close the previous Rviz terminal and in new one, run :`
```
roslaunch ifollow_test ifollow_view.launch map_file:=$HOME/ros1ws_ifollow_test/maps/map.yaml
```
![alt text](../../pictures/mapping.png "Simulation")

**Note :** The command above also shows the stack of localisation and naviguation.

**(2)- After control the robot through teleoperation (with keyboard),** `run in new terminal` : 
```
> rosrun turtlebot3_teleop turtlebot3_teleop_key
```
Then follow the guide to move the robot inside the map.

**(3)- Then control the robot inside a map through nav goal 2D, perform in different terminal the above step** `(2)` **and in** `RvIz`:

```
> `Set the initial pose of the robot inside the map build by pushing the button 2D Pose Estimate`. This set the position and orientation of the robot through one topic.

> Use `teleoperation to adjust` the localisation of the robot. This reduce the number of green arrow showing by AMCL.

> `Set the goal to the robot by pushing the button 2D Nav Goal.` Then the robot follow the path.
```

## Support
Please contact Nassim Sadikou for more details : mcnass2@gmail.com
