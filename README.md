# ros1w_ifollow_test

![alt text](pictures/ifollow.png "iFollow logo (copyright)")

# Description
`ros1w_ifollow_test` is the workspace allowing to control a Turtlebot3 model burger.

## OS verification
Windows user
```
1- Install any virtual machine. By exemple virtualBox at http://virtualbow.org/wiki/Downloads

2- Downlaod an OS and install it on the virtual machine. Here is the Linux 18.04 link in the official website https://releases.ubuntu.com/18.04/
```

Linux user :
```
Skip the windows user process. Ensure to have ubuntu at 18.04 or 20.04 version
```

# Installation
Clone the repository
```
$ git clone https://gitlab.com/Mcnasss/ros1ws_ifollow_test.git
```

`Install ROS`
```
Follow the guide here : http://wiki.ros.org/ROS/Installation
```
**Remark :** For Noetic version, ubuntu 20.04 is required. Also, include rosdep installation in the process.`

`Environment setup`
```
1- Setup the OS environment for th software
$ cd ~
$ echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
$ echo "source ~/ros1ws_ifollow_test/devel/setup.bash" >> ~/.bashrc
```
**Important :** In order to launch your software, every new terminal should be sourced. In order to facilitate that, the process above source automatically your ROS environment in each new terminal.

```
2- ROS configurations to handle local-remote server & TurtleBot model.
$ echo "export ROS_MASTER_URI=http://localhost:11311" >> ~/.bashrc
$ echo "export TURTLEBOT3_MODEL=burger"  >> ~/.bashrc
$ source ~/.bashrc
```
**Note :** To check if it's working :
```
$ nano .bashrc
$ [Ctrl + x]
```

`Install some libraries required and python3 dependancies`
```
$ cd ros1ws_ifollow_test
$ bash init_env.sh
```

## ROS dependances
3- To install the specific dependencies untracked for the workspace, at workspace root, run `rosdep install --from-paths src --ignore-src -r -y`

# Compilation
```
$ catkin_make

$ catkin_make install

$ source devel/setup.bash 
```

# Execution
`To launch the whole workspace, in a terminal :`
```
$ roslaunch ifollow_test ifollow.launch map_file:=$HOME/ros1ws_ifollow_test/maps/map.yaml
```
`To perform web teleop through MQTT, in a terminal :`
```
$ python3 ros1ws_ifollow_test/src/ifollow_test/remote_teleop.py -host localhost -port 1883 -out cmd_mqtt_out -in cmd_mqtt_in
```
`To perform local teleop through ROS, in a terminal : `
```
$ rostopic pub -r 1 /cmd_local geometry_msgs/Twist "linear:
  x: 1.0
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 4.0"
```
`To trigger AprilTag detection for each artag & send nav goal, first stop both /cmd_local and /cmd_webb and in a new terminal : `
```
$ rostopic pub /artag_goal std_msgs/Bool "data: true"
```
**Note :** `Each package of the workspace can be launch independantly.  In order to do that, clone the package in a workspace an follow the instructions.`


# Contents
**0- ifollow_test (OK)**
This is the parent package which gather all other package to make the software simulation work. `src/ifollow_test`

**1- Setting test environment (OK)**
The first step is to set the simulation environnement. Also it provides instruction to control turlebot3. `Please for the instructions in the directory src/environment_setting`

**2- Multiplexer command (OK)**
To multiplex command from different input, `please follow the instructions in the directory src/multiplexer`

**# 3- Remote teleoperation (OK)**
To perform a remote teleoperation out of ROS, `please follow the instructions in the directory src/remote_teleoperation`

**# 4- 5 Visual tag goal &&  features use (OK)**
To send a nav goal through visual tag like AprilTag, `please refer to the instructions in the directory src/visual_tag_goal` The package also perform the calculation of the AprilTag orientation based on the tag. `This basic implementation wait until (dim_thresh * 10 secs) to send the next goal tag`

**# 6- Image capture use (KO), no device camera)**

